#!/usr/bin/env python3
#coding:utf-8

#author by: wenbronk

'''
CGI(Common Gateway Interface),通用网关接口,它是一段程序,运行在服务器上如：HTTP服务器，提供同客户端HTML页面的接口。

CGI程序可以是Python脚本，PERL脚本，SHELL脚本，C或者C++程序等。

'''
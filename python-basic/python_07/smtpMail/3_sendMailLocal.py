#!/usr/bin/env python3
#coding:utf-8

#author by: wenbronk

'''
python 发送邮件, 本地服务器

'''

import smtplib
from email.mime.text import MIMEText
from email.header import Header


sender = 'from@runoob.com'
receivers = ['421890856@qq.com']

message = MIMEText('python 邮件发送测试', 'plain', 'utf-8')
message['From'] = Header('菜鸟教程', 'utf-8')
message['To'] = Header('ceshi', 'utf-8')

subject = 'python smtp 邮件测试'
message['Subject'] = Header(subject, 'utf-8')

try:
    smtpObj = smtplib.SMTP('localhost')
    smtpObj.sendmail(sender, receivers, message.as_string())
    print('邮件发送成功')
except smtplib.SMTPException:
    print('无法发送邮件')




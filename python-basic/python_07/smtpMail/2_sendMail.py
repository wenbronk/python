#!/usr/bin/env python3
#coding:utf-8

#author by: wenbronk

'''
使用python发送邮件的小案例

'''

import smtplib
from email.mime.text import MIMEText
from email.header import Header

# 第三方smtp服务
mail_host = 'smtp.163.com'
mail_user = 'wenbronk@163.com'
mail_pass = 'a75767626'

sender = 'wenbronk@163.com'
# 接收邮件
receives = ['wenbronk@126.com']

message = MIMEText('Python 邮件发送测试', 'plain', 'utf-8')
message['From'] = Header('hello email', 'utf-8')
message['To'] = Header('test', 'utf-8')

subject = 'Python SMTP 邮件测试'
message['Subject'] = Header(subject, 'utf-8')

try:
    smtpObj = smtplib.SMTP()
    smtpObj.connect(mail_host, 25)
    smtpObj.login(mail_user, mail_pass)
    smtpObj.sendmail(sender, receives, message.as_string())
    print('邮件发送成功')
except smtplib.SMTPException:
    print('邮件发送失败')



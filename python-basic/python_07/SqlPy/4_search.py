#!/usr/bin/env python3
#coding:utf-8

#author by: wenbronk

'''
查询操作,
fetchone(), 获取下一个查询结果集, 集合是个对象
fetchall(), 接受全部返回行
rowcount: 只读属性, 返回执行 execute() 方法后影响的的行数
'''

import pymysql

db = pymysql.connect('localhost', 'root', 'root', 'iwhere')
cursor = db.cursor()

sql = "select * from python \
      where income > '%d'" % (1000)

try:
    cursor.execute(sql)
    results = cursor.fetchall()

    for row in results:
        fname = row[0]
        lname = row[1]
        age = row[2]
        sex = row[3]
        income = row[4]
        print(fname, lname, age, sex, income)
except:
    print('error: unable to fetch data')
    pass


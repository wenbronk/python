#!/usr/bin/env python3
#coding:utf-8

#author by: wenbronk

'''

需要安装pysql, pip install PyMySQL

'''

#链接mysql
import pymysql
db = pymysql.connect('localhost', 'root', 'root', 'iwhere')

# 创建一个cursor()
cursor = db.cursor()

# 使用execute() 执行sql语句
result = cursor.execute('select version()')
print(result)

# 使用 fetchone() 方法获取单条数据
data = cursor.fetchone()
# 关闭mysql连接


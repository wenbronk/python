#!/usr/bin/env python3
#coding:utf-8

#author by: wenbronk

'''
数据库插入值操作

'''

import pymysql
username = 'root'
passwd = 'root'
database = 'iwhere'
db = pymysql.connect('localhost', username, passwd, database)
cursor = db.cursor()


# sql = """
#     insert into python (first_name, last_name, age, sex, income) values
#     ('Mac', 'Mohan', 20, 'M', 2000)
# """

# 或者写成如下形式的
# sql = "INSERT INTO EMPLOYEE(FIRST_NAME, \
#        LAST_NAME, AGE, SEX, INCOME) \
#        VALUES ('%s', '%s', '%d', '%c', '%d' )" % \
#        ('Mac', 'Mohan', 20, 'M', 2000)

# 使用变量取值的时候
first_name = 'vini'
last_name = 'h'
sql = "INSERT INTO EMPLOYEE(FIRST_NAME, \
       LAST_NAME, AGE, SEX, INCOME) \
       VALUES ('%s', '%s', '%d', '%c', '%d' )" % \
       ('Mac', 'Mohan', 20, 'M', 2000)
try:
    cursor.execute(sql)
    # cursor.execute('insert into python (first_name, last_name) values ("%s", "%s")' % \ (first_name, last_name))
    db.commit()
except:
    # 发生异常回滚
    db.rollback()
db.close()


# 带参数的sql语句这样玩

sql = "insert into python (first_name, last_name) values \
      ('%s', '%s')" % (first_name, last_name)
#!/usr/bin/env python3
#coding:utf-8

#author by: wenbronk

'''
使用python 创建表

'''

import pymysql

db = pymysql.connect('localhost', 'root','root', 'iwhere')
cursor = db.cursor()

cursor.execute('drop table if EXISTS python')

sql = '''
        create table python (
        first_name char(20) not null,
        last_name char(20),
        age int,
        sex char(1),
        income FLOAT )
      '''
cursor.execute(sql)
db.close()




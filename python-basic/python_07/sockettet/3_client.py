#!/usr/bin/env python3
#coding:utf-8

#author by: wenbronk

'''
客户端连接,

socket.connect(hostname, port)
打开一个tcp链接, 然后就可以获取数据了

'''

import socket, sys
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# 绑定端口
host = socket.gethostname()
port = 9999
s.connect((host, port))

# 接受小于 1024 的字节数据
msg = s.recv(1024)

s.close()

print(msg.decode('utf-8'))


#!/usr/bin/env python3
#coding:utf-8

#author by: wenbronk

'''
使用server编写一个服务端程序

'''

import socket, sys

# 创建sockerserver对象
serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# 绑定端口
host = socket.gethostname()
port = 9999
serversocket.bind((host, port))

# 设置最大连接数, 5个, 多了排队
serversocket.listen(5)

while True:
    clientsocket, addr = serversocket.accept()
    print('链接地址是: %s' % str(addr))

    msg = 'welcome!!'
    clientsocket.send(msg.encode('utf-8'))
    clientsocket.close()



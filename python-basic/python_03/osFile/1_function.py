#!/usr/bin/env python3
#coding:utf-8

'''
os 模块提供了非常丰富的方法来处理文件和目录

access(path, mode)          校验权限模式
chdir(path)                 改变当前工作目录
chflags(path, flags)        设置路径的标记为数字标记
chmod(path, mode)           更改权限
chown(path, uid, gid)       更改文件所有者
chroot(path)                改变当前进程的目录
...


'''
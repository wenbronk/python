#!/usr/bin/env python3
#coding:utf-8

'''
open(file, mode) 会返回一个file对象, 可以进行文件的读写操作
    mode 分为: r, w, a
            (r, rb, r+, rb+), (w, wb, w+, wb+), (a, ab, a+, ab+)

read(size)  读取指定字节, 不传全读取
readline()      读取一行, 读到 \n, 返回一个空串说明读到最后了
readlines(size)     读取n多行, 如果不传, 则读取全部, 返回数组


'''

# 使用open打开一个文件
file = open('../source/file.txt', 'w')
file.write('python is a better language, \nyes, it is so good\ni like it')
file.close()

file = open('../source/file.txt', 'r')
str = file.read()
print(str)
file.close()

file = open('../source/file.txt', 'r')
str = file.readline()
print(str)
file.close()

file = open('../source/file.txt', 'r')
list = file.readlines()
print(list)
file.close()

#还可以迭代读取
file = open('../source/file.txt', 'r')
for line in file:
    print(line)
file.close()


#!/usr/bin/env python3
#coding:utf-8

'''
写入文件中

tell()      返回文件对象所处的位置,
seek(offset, from_what)      改变文件的位置, 可以使用seek()
truncate([size])    从首字截取size个字符, 无size标识从当前位置阶段
write(str)          将字符串写入
writelines(sequence)    写入一个序列, 需要换行则添加自己的换行符

'''

file = open ('../source/file.txt', 'w')
num = file.write('you can you up\nno can no bb\nszww')
print(num)
file.close()






#!/usr/bin/env python3
#coding:utf-8

'''
format 的使用

使用repr输出一个表
'''

# 使用repr输出一个表
    #rjust() 字符串靠右, 并在左侧填充空格, ljust(), center()
for x in range(1, 11):
    print(repr(x).rjust(2), repr(x**2).rjust(3), end=' ')
    print(repr(x**3).rjust(4))

# 使用format完成同样事情, 冒号后的整数, 表示留白
for x in range(1, 11):
    print('{0:2d} {1:3d} {2:4d}'.format(x, x**2, x**3))

#format使用
print('{} 网址 是 {}'.format('blog', 'wenbronk.blog.cn'))

# 可使用index 确定位置
print('{1} 和 {1}, 还有 {0}, {other}'.format('google', 'runnbo', other='nihao'))

# 冒号后可以规范输出
table = {'goole': 1, 'runnoob': 2, 'taobao': 3}
for name, number in table.items():
    print('{name:10} ==> {number:10}'.format(name = name, number = number))

# 类c字符串格式化
print('我是 c 语言的格式化 %s' % 'hahhah')

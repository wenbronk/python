#!/usr/bin/env python3
#coding:utf-8

'''
python 的输出

输出的3种方式:
    print(), 表达式语句, file的write() 方法

    str.format() 可以格式化输出,
    str(): 函数返回一个用户易读的表达形式,

    repr(): 产生一个解释器易读的表达形式, 可以转义字符串中的特殊字符
            参数可以是python的任意对象

'''


s = 'Hello Runoob'
print(str(s))
print(repr(s))






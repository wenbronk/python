#!/bin/bash/env python
#!coding=utf-8
import ConfigParser
import threading

from rediscluster import StrictRedisCluster

def test_radis():
    # conf = ConfigParser.SafeConfigParser()
    # conf.read('G://Programs//RecommendationSystem//Recommen//trunk//recommendationsystem//resources//properties.cfg')
    # pw = conf.get('redis-cluster', 'PW')
    # start_nodes = [{'host': conf.get('redis-cluster', 'HOST1'), 'port': conf.getint('redis-cluster', 'PORT1')},
    #                {'host': conf.get('redis-cluster', 'HOST2'), 'port': conf.getint('redis-cluster', 'PORT2')},
    #                {'host': conf.get('redis-cluster', 'HOST3'), 'port': conf.getint('redis-cluster', 'PORT3')},
    #                {'host': conf.get('redis-cluster', 'HOST4'), 'port': conf.getint('redis-cluster', 'PORT4')},
    #                {'host': conf.get('redis-cluster', 'HOST5'), 'port': conf.getint('redis-cluster', 'PORT5')},
    #                {'host': conf.get('redis-cluster', 'HOST6'), 'port': conf.getint('redis-cluster', 'PORT6')}]
    server_ip = '192.168.50.202'
    startup_nodes = [{"host": server_ip, "port": i} for i in xrange(7000, 7006)]
    pw = 'bd86d2ab-08e5-4ae0-87ce-06cc021336d0'
    re = StrictRedisCluster(startup_nodes=startup_nodes, readonly_mode=True, decode_responses=True, password=pw)
    # key = 'sortPoi:01:10-03-1498275130188:uuuussssseeeerrrrIIIIdddd'
    # key = 'sortPoi:01:10-03-1498888886399:uuuussssseeeerrrrIIIIdddd'
    key = 'sortPoi:01:10-03-1498888:uuuussssseeeerrrr'
    for i in xrange(100) :
        if not re.exists(key) and re.llen(key) != 0:
            print 'no key '
            return
        if re.llen(key) == 1:
            redis_data = re.lrange(key, 0, 0)
        else:
            redis_data = re.lrange(key, 0, -1)
        if len(redis_data) == 0:
            print '---NO DATA'
        else:
            print "111", len(redis_data)

if __name__ == '__main__':
    ts = []
    for _ in range(100):
        ts.append(threading.Thread(target=test_radis))
    for t in ts:
        t.start()

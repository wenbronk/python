#!/bin/bash/env python
#coding=utf-8

# 测试循环读取redis的问题

import os, sys, time, traceback
import redis, threadpool
from rediscluster import StrictRedisCluster

count = 0
def main():
    server_ip = '192.168.50.202'
    redis_nodes = [{"host": server_ip, "port": port} for port in xrange(7000, 7006)]
    try :
        redis = StrictRedisCluster(startup_nodes=redis_nodes, password='bd86d2ab-08e5-4ae0-87ce-06cc021336d0', max_connections=100)
        print("connect success")
    except Exception, err:
        print err
        print 'failed to connect cluster'
        sys.exit(0)

    task_pool = threadpool.ThreadPool(100)
    # 构建for循环
    task_list = []
    for i in xrange(100) :
        task_list.append(readFromRedis(redis))

    if (task_pool.putRequest() == None) :
        return
    map(task_pool.putRequest, task_list)
    # print map
    task_pool.poll()

def readFromRedis(redis):
    for i in xrange (100) :
        length = redis.llen('sortPoi:01:10-03-1498275130188:uuuussssseeeerrrrIIIIdddd')
        print (length)

if __name__=='__main__':
    main()



import json
from redis.connection import ResponseError
import ConfigParser

from flask import request
from flask import jsonify
from rediscluster import StrictRedisCluster

from recommendationsystem import app
from recommendationsystem.data.data_preprocess import parse_data
import recommendationsystem.process.sort as recom_sort
from recommendationsystem import PROPERTY_FILE

conf = ConfigParser.SafeConfigParser()
conf.read(PROPERTY_FILE)
db = conf.get('redis', 'DB')
pw = conf.get('redis', 'PW')
start_nodes = [{'host': conf.get('redis', 'HOST1'), 'port': conf.getint('redis', 'PORT1')},
               {'host': conf.get('redis', 'HOST2'), 'port': conf.getint('redis', 'PORT2')},
               {'host': conf.get('redis', 'HOST3'), 'port': conf.getint('redis', 'PORT3')},
               {'host': conf.get('redis', 'HOST4'), 'port': conf.getint('redis', 'PORT4')},
               {'host': conf.get('redis', 'HOST5'), 'port': conf.getint('redis', 'PORT5')},
               {'host': conf.get('redis', 'HOST6'), 'port': conf.getint('redis', 'PORT6')}]
re = StrictRedisCluster(startup_nodes=start_nodes, readonly_mode=True, decode_responses=True, password=pw)


@app.route('/sort', methods=['POST', 'GET'])
def sort():
    """
     test url :
     http://0.0.0.0:5006/sort?redis_key=&hour=1&user_lat=40&user_lng=116&pre_lat=40&pre_lng=116&return_num=10&mode=0201

    """
    params_info = str(request.args.to_dict()) or str(request.form.to_dict()) or request.data
    try:
        params = dict()
        required_params = ['hour', 'user_lat', 'user_lng', 'pre_lat', 'pre_lng', 'return_num', 'mode', 'redis_key']
        lack_param_info = "miss one or more params"
        if request.method == 'POST':
            params['hour'] = int(request.form['hour'].encode("utf-8"))
            params['user_lat'] = float(request.form['user_lat'].encode("utf-8"))
            params['user_lng'] = float(request.form['user_lng'].encode("utf-8"))
            params['pre_lat'] = float(request.form['pre_lat'].encode("utf-8"))
            params['pre_lng'] = float(request.form['pre_lng'].encode("utf-8"))
            params['return_num'] = int(request.form['return_num'].encode('utf-8'))
            params['mode'] = str(request.form['mode'].encode("utf-8"))
            params['redis_key'] = request.form['redis_key'].encode("utf-8")
        else:
            args = request.args
            keys = args.keys()
            for item in required_params:
                if item not in keys:
                    return jsonify(status=401, info=lack_param_info + " especially for " + item)
            params['hour'] = int(args.get('hour').encode("utf-8"))
            params['user_lat'] = float(args.get('user_lat').encode("utf-8"))
            params['user_lng'] = float(args.get('user_lng').encode("utf-8"))
            params['pre_lat'] = float(args.get('pre_lat').encode("utf-8"))
            params['pre_lng'] = float(args.get('pre_lng').encode("utf-8"))
            params['return_num'] = int(args.get('return_num').encode('utf-8'))
            params['mode'] = str(args.get('mode').encode("utf-8"))
            params['redis_key'] = args.get('redis_key').encode("utf-8")

        # re = redis.Redis(host=REDIS.HOST, port=REDIS.PORT, db=REDIS.DB, password=REDIS.PASSWORD)
        datalist = []
        keys = params.get('redis_key').split(',')
        for key in keys:
            if not re.exists(key) and re.llen(key) != 0:
                continue
            if re.llen(key) == 1:
                redis_data = re.lrange(key, 0, 0)
            else:
                redis_data = re.lrange(key, 0, -1)
            for item in redis_data:
                datalist.append(eval(item))

        if len(datalist) == 0:
            return jsonify(status=402, info='no data in redis')

        data = parse_data(datalist, params['mode'])

        result = dict()
        # app.logger.info('- -: [%s] %s "%s" before_sort: %s' %
        #                 (request.access_route[0], request.method, params_info, data))
        if params['mode'] in ['0101', '0102']:
            result = recom_sort.data_sort(data=data, hour=params['hour'], user_lat=params['user_lat'],
                                          user_lng=params['user_lng'], pre_lat=params['pre_lat'],
                                          pre_lng=params['pre_lng'],return_num=params['return_num'])
        elif params['mode'] in ['0201', '0202']:
            result = recom_sort.gaode_sort(data=data, user_lat=params['user_lat'], user_lng=params['user_lng'],
                                           pre_lat=params['pre_lat'], pre_lng=params['pre_lng'],
                                           return_num=params['return_num'])
        app.logger.info('- -: [%s] %s "%s" after_sort: %s' %
                        (request.access_route[0], request.method, params_info, result))
        return jsonify(status=200, info="success", data=result)
    except KeyError, e:
        app.logger.error("=====KeyError " + str(e.message) + "========")
        app.logger.error('- -: [%s] %s "%s" ' % (request.access_route[0], request.method, params_info))
        return jsonify(status=401, info='miss parameters or columns')
    except ValueError, e:
        app.logger.error("=====ValueError " + str(e.message) + "========")
        app.logger.error('- -: [%s] %s "%s" ' % (request.access_route[0], request.method, params_info))
        return jsonify(status=401, info='wrong value of data')
    except AttributeError, e:
        app.logger.error("=====AttributeError " + str(e.message) + "========")
        app.logger.error('- -: [%s] %s "%s" ' % (request.access_route[0], request.method, params_info))
        return jsonify(status=401, info='miss parameters or columns')
    except TypeError, e:
        app.logger.error("=====TypeError " + str(e.message) + "========")
        app.logger.error('- -: [%s] %s "%s" ' % (request.access_route[0], request.method, params_info))
        return jsonify(status=401, info='data error')
    except ResponseError, e:
        app.logger.error("=====ResponseError " + e.args[0] + "========")
        app.logger.error('- -: [%s] %s "%s" ' % (request.access_route[0], request.method, params_info))
        return jsonify(status=403, info='connect redis error')


@app.route('/sort/json', methods=['POST'])
def sort_data_directly():
    if request.method == 'POST':
        try:
            if not request.mimetype == 'application/json':
                return jsonify(status=405, info='content-type should be application/json')
            params = json.loads(request.data)
            required_params = ['hour', 'user_lat', 'user_lng', 'pre_lat', 'pre_lng', 'return_num', 'mode', 'data_list']
            for key in required_params:
                if key not in params.keys():
                    return jsonify(status=401, info='miss parameters:' + key)
            datalist = params['data_list']
            datalist = eval(datalist)
            mode = params['mode'].encode('utf-8')
            data = parse_data(datalist, mode=mode)
            result = dict()
            if mode == '0101' or '0102':
                result = recom_sort.data_sort(data=data, hour=params['hour'], user_lat=params['user_lat'],
                                              user_lng=params['user_lng'], pre_lat=params['pre_lat'],
                                              pre_lng=params['pre_lng'], return_num=params['return_num'])
            elif mode == '0201' or '0202':
                result = recom_sort.gaode_sort(data=data, user_lat=params['user_lat'], user_lng=params['user_lng'],
                                               pre_lat=params['pre_lat'], pre_lng=params['pre_lng'],
                                               return_num=params['return_num'])
            return jsonify(status=200, info='success', data=result)
        except KeyError, e:
            app.logger.info("=====KeyError" + str(e.message) + "========")
            return jsonify(status=401, info='miss parameters or columns')
        except ValueError, e:
            app.logger.info("=====ValueError" + str(e.message) + "========")
            return jsonify(status=401, info='wrong value of data')
        except AttributeError, e:
            app.logger.info("=====AttributeError" + str(e.message) + "========")
            return jsonify(status=401, info='miss parameters or columns')
        except TypeError, e:
            app.logger.info("=====TypeError" + str(e.message) + "========")


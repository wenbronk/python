#!/bin/bash/env python
#coding=utf-8

# for i in xrange (100) :
#     print i


# length = 100
#
# while length > 0 :
#     print length
#     length -= 1

def sumStartToEnd(start, end):
    sum = 0
    for n in range(start, end + 1, 1):
        sum = sum + n
    return sum


if __name__ == '__main__' :
    print(sumStartToEnd(1, 10000))

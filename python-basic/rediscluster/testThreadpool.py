#!/bin/bash/env python
#coding=utf-8

# 测试线程池使用
import threadpool,time

def sayhello(str):
    print 'hello ', str

name_list = {'xiaozi', 'aa', 'bb', 'cc'}
pool = threadpool.ThreadPool(100)
start_time = time.time()
requests = threadpool.makeRequests(sayhello, name_list)
[pool.putRequest(req) for req in requests]
pool.wait()
print '%d second' % (time.time() - start_time)
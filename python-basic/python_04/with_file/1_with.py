#!/usr/bin/env python3
#coding:utf-8

'''
使用with, 可以保证被打开的队形使用完毕后正确执行清理方法
    预定义的清理行为

'''

with open('../source/test.search') as file:
    for line in file:
        print(line, end="\t")


#!/usr/bin/env python3
#coding:utf-8

'''
异常处理机制

    一个exception里面可以处理多个异常
    可以多个exception并列
    最后一个exception忽略名字, 可用于通配使用,
    最后可跟一个else: 没有任何异常的时候执行

    同样最后有   finally模块, 如果异常抛出后, 没有任何except拦击额, 那么最终在finally中被抛出

'''

import sys

while True:
    try:
        x = int(input('Please enter a number: '))
        # 可以使用break跳出
        # break
    except ValueError as err:
        print('Oops! That was no valid number. Try again ', err)
    except (RuntimeError, TypeError, NameError) :
        pass
    except :
        print("unexcepted error: ", sys.exc_info()[0])
    else:
        print(x, 'your input number is ')
        break
    finally :
        print('i am ok!!')



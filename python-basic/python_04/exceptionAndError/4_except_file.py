#!/usr/bin/env python3
#coding:utf-8

'''
一个更加复杂的try..catch
'''

def divide(x, y):
    try:
        result = x / y
    except ZeroDivisionError:
        print('division by zero !')
    else :
        print('results is', result)
    finally:
        print('executing finally clause! ')


divide(1, 0)


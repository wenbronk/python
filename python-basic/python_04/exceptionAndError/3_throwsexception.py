#!/usr/bin/env python3
#coding:utf-8

'''
python中使用 raise 抛出异常

如果捕捉到一个异常, 但不想处理, 可以简单的使用raise再次抛出

自定义异常, 可以创建新的exception来拥有自己的异常, 应继承自Exception类
如果一个模块抛出多个不同异常, 那么闲创建一个基础异常类, 然后所有的异常类继承自这个基类

'''

try :
    if (1 == 1):
        raise NameError('HiThere')
except NameError as err:
    print('an exception flew by! ', err)
    # raise


# 使用自定义异常
from exceptionAndError import myError
try:
    raise myError('123')
except myError as e:
    print("your excpetion is ", e.value)



#!/usr/bin/env python3
#coding:utf-8

'''
自定义异常
'''

class myError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
    




#!/usr/bin/env python3
#coding:utf-8

'''
异常:         语法异常, 编译器会报
              运行异常:

'''

# ZeroDivisionError: division by zero
# a = 1 / 0

#NameError: name 'spam' is not defined
# spam * 3

# TypeError: must be str, not int
# '2' + 2


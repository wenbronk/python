#!/usr/bin/env python3
#coding:utf-8

#author by: wenbronk

'''
python的多线程

python3种线程可用2个模块:
_thread: (废弃)      _thread.start_new_thread ( function, args[, kwargs] )
threading

'''

import _thread
import time

# 为线程定义个函数:
def print_time(threadName, delay):
    count = 0
    while count < 5:
        time.sleep(delay)
        count += 1
        print('%s: %s' % (threadName, time.ctime(time.time())))


# 创建2个线程
try:
    _thread.start_new_thread(print_time, ('Thread-1', 2, ))
    _thread.start_new_thread(print_time, ('th---2', 4, ))
    print('success')
except:
    print('无法创建线程')

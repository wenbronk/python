#!/usr/bin/env python3
#coding:utf-8

#author by: wenbronk

'''
python的线程池, 需要安装, pip install threadpool

'''

import threadpool,time

def sayhello(str):
    print('hello ', str)

name_list = {'xiaozi', 'aa', 'bb', 'cc'}
pool = threadpool.ThreadPool(100)
start_time = time.time()

# 要创建多线程的函数, 参数, 及回掉(可以不写)
requests = threadpool.makeRequests(sayhello, name_list)

# 将所有req put进线程池执行
[pool.putRequest(req) for req in requests]

# 等到所有线程完事退出
pool.wait()
print('%d second' % (time.time() - start_time))
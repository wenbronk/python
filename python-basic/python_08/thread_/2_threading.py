#!/usr/bin/env python3
#coding:utf-8

#author by: wenbronk

'''
线程模块

_thread提供了一个低级别的, 原始的线程及一个简单的锁,

threding还提供了 :
    threading.currentThread(); 返回当前线程的变量
    threading.enumerate() 返回一个包含正在运行的线程list,
        正在运行指线程启动后, 姐宿迁, 不包括启动前和终止后的线程

提供了Thread类来处理线程
Thread:
    run(), 表示线程活动的方法
    start(), 启动线程
    join([time]) 等待至线程终止, 阻塞调用线程知道线程的join()被调用终止
    isAlive() 返回线程是否活动
    getname()   返回县城名字
    setName()   设置线程名字

'''

import threading
import time

exitFlag = 0

class myThread(threading.Thread):
    def __init__(self, threadId, name, counter):
        threading.Thread.__init__(self)
        self.threadId = threadId
        self.name = name
        self.counter = counter

    def run(self):
        print('thread start', self.name)
        print_time(self.name, self.counter, 5)
        print('退出线程: ', self.name)

def print_time(threadName, delay, counter):
    while counter:
        if exitFlag:
            threadName.exit()
        time.sleep(delay)
        print('%s: %s' % (threadName, time.ctime(time.time())))
        counter -= 1

# 创建2个线程
thread1 = myThread(1, 't--1', 1)
thread2 = myThread(2, 'thre2', 2)

# 开启线程
thread1.start()
thread2.start()
thread1.join()
thread2.join()

print('exiting thread')


#!/usr/bin/env python
#coding:utf-8

'''
循环分为 for, while, 嵌套循环
'''

# while 循环
numbers = [12, 23, 45, 78,28, 3]
even = []
odd = []
while len(numbers) > 0:
    number = numbers.pop()
    if (number % 2 == 0):
        even.append(number)
    else:
        odd.append(number)

print even
print odd

# 无限循环
i = 0
while True:
    print i
    i += 1


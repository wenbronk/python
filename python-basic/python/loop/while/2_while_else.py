#!/usr/bin/env python
#coding:utf-8

'''
while 中使用else, 条件不满足则执行else
正常执行完成, 不是break跳出的情况
'''

count = 0
while count < 5:
    print count
    count += 1
else :
    print ('end')
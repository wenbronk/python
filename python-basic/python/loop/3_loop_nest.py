#!/usr/bin/env python
#coding:utf-8

'''
循环嵌套
'''

i = 2
while (i < 100):
    j = 2
    while (j <= (i/j)):
        print i % j
        if not (i%j): break
        j += 1
    if (j > i/j):
        print i, '质数'
    i += 1


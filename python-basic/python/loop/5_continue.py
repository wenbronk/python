#!/usr/bin/env python
#coding:utf-8

'''
continue 语句, 和java一样
跳过当前循环, 进行下一次循环
'''

for i in ['a', 'b', 'c', 'd']:
    if i == 'c':
        continue
    print("letter, $i"), i






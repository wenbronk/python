#!/usr/bin/env python3
#coding:utf-8

'''
元组, 不可被修改
可以用 + 拼接

'''

tuple = ('ab', 123, 'run', 'a')

# 可以用角标索引
print(tuple[-1])

# 不可被修改
# tuple[1] = 2

print(tuple)
#!/usr/bin/env python3
#coding:utf-8

'''
无序不重复的序列
用 {}, 或 set() 构建, 空的只能用 set(), {} 用来构建空字典
集合运算
 - 差集 , | 并集 , & 交集 , ^ 补集
'''

student = {'Tom', 'Mary', 'Jack', 'Rose', 'Tom'}

# 成员测试
if ('Rose' in student):
    print('Rose', 'in student')

a = set('abcdsegs')
b = set('agqbfsas')

print(a - b)
print(a | b)
print(a & b)
print(a ^ b)


print(student)
#!/usr/bin/env python3
#coding:utf-8

'''
list使用最频繁
截取,
可替换
可被索引及切片
可用 + 拼接

'''

alist = ['a', 'b', 'e', 'f', 'g']

# 截取, :
subList = alist[2:5]

# 输出2次列表
print(subList * 2)

# 列表元素可替换
subList[1:3] = [1, 2, 3]


print(subList)

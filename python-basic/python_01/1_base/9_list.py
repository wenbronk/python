#!/usr/bin/env python3
#coding:utf-8

'''
列表, 是python种最常用的序列类型
所有序列支持 索引, 切片, 加, 乘, 检查成员
[:] 截取
[]  角标取值

函数:
len(list) 长度
max(list) 最大值       只能用在 类型统一的列表中
min(list) 最小值
list(seq)  将元组转化为列表


方法:
append(obj) 列表末尾添加
count(obj)  统计某个元素出现的次数
extends(seq) 在列表末尾添加多个值
index(obj)  返回值的index
pop(obj)    弹出元素, 默认最后一个
insert(index, obj) 在某个位置插入
remove(obj)     移除列表中某个值
reverse()       反向列表
sort([func])    排序
clear()         清空
copy()          复制
'''

aList = ['1', 'a', 3, 'abc', 'test', 56]

print(aList[2:4])
# 删除
del aList[2]

# 函数
print(len(aList))
# print(max(aList))
print(list(('1', 2, "3")))

# 方法
aList.append('abc')
count = aList.count('abc')
index = aList.index('abc')
aList.extend('56')
aList.insert(0, 'a')
element = aList.pop(2)
aList.remove('abc')
aList.reverse()
# aList.sort()
aList.clear()

print(aList)
print(count)
print("element", element)


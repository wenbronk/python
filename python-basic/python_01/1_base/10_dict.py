#!/usr/bin/env python3
#coding:utf-8

'''
字典
dict['key']        取值
dict['key'] = value 赋值
del dict['key']     删除键值对
dict.clear()        清空
del dict            删除整个字典

len(dict)           计算key个数
str(dict)           转成可输出的字符串

fromkeys()      创建新的字典, 值为初始值
get(key, default=None)  返回值
setdefault(key, default=None)   设置默认值
key in dict     判断是否存在
items()         以列表返回可遍历的元祖
keys()          返回所有建
values()        返回所有的值
update(dict2)   将dict2更新到dict里面
pop(key[default])   删除字典对应的key
popitem()       随机返回并删除字典中一对键值对

'''

dict = {'name': 'vini', 'age': 25}

dict['addr'] = 'bj'

del dict['name']

print(dict)

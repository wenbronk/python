#!/usr/bin/env python3
#coding:utf-8

'''
python中使用变量不需要声明, 但使用前必须赋值
变量是没有类型的, 类型是指变量中内存对象的类型

标准数据类型: Number, String, List, Tuple, Sets, Dictionary

number 支持int , float, bool, comples

'''

a =  b = c = 1
d, e, f, = 2, 4, 5


print(a,b,c)
print(d, e, f)
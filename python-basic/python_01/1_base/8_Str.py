#!/usr/bin/env python3
#coding:utf-8

'''
字符串操作
转义字符  \

字符串运算:
    + 字符串连接
    * 重复输出
    [] 索引获取字符
    [: } 截取
    in 是否包含
    not in
    r''   不专意直接输出, 元样子输出
    % 格式化   %c, %s, %d, %o, %x, %X, %e, %g, %p
'''

a = 'Hello'
b = 'Python'

print(a + b)
print()

print(r'\n')

# 字符串格式化输出
print ('my name is %s, my age is %d' % ('小明', 10))


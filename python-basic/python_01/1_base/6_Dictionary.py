#!/usr/bin/env python3
#coding:utf-8

'''
字典, 查询速度不受大小限制
取值, dict['key']

dict.keys()   所有键
dict.values() 所有值

'''

dict = {}  # 构造空字典
dict['one'] = 'vini'
dict[2] = '2'

# 可遍历
for obj in dict :
    print(obj)

print(dict)
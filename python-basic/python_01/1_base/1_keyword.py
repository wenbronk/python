#!/usr/bin/env python3
#coding:utf-8

'''
python保留关键字
'''

import keyword

print(keyword.kwlist)


# 多行语句, 使用 \, 在[] 或 {}, () 中的语句, 不需要使用
total = 'abc' + \
        'bcd'

array = ['a', 'b',
         'c']

tuplea = ('a', 'b',
          'c')

print(total)
#!/usr/bin/env python3
#coding:utf-8

'''
多继承
方法同名时, 默认调用在括号中排前的父方法

'''

class People:
    # 基本属性
    name = ''
    age = 1

    # 定义私有变量
    _weight = 0

    # 构造方法
    def __init__(self, name, age, weight):
        self.name = name
        self.age = age
        self._weight = weight

    #method:
    def speak(self):
        print('{0}说: 我{1}岁了'.format(self.name, self.age))
        return self

# 单继承实例
class Student(People):
    grade = 0
    def __init__(self, name, age, weight, grade):
        # 调用父类构造函数
        People.__init__(self, name, age, weight)
        self.grade = grade

    def speak(self):
        print('{0}说, 我今年{1}岁, 我在都{2}年纪'.format(self.name,
            self.age, self.grade))
        return self

# 多继承示例
# 多继承前的准备
class Speaker():
    topic = ''
    name = ''
    def __init__(self, name, topic):
        self.name = name
        self.topic = topic
    def speak(self):
        print('我叫{}, 我是一个演说家, 主题是{}'.format(self.name, self.topic))

# 多继承
class Sample(Speaker, Student):
    age = '23'
    def __init__(self, name, age, weight, grade, topic):
        Student.__init__(name, age, weight, grade)
        Speaker.__init__(name, topic)

test = Sample('vini', 25, 48, 3, 'Python')
test.speak()





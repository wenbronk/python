#!/usr/bin/env python3
#coding:utf-8

'''
用来python 的继承
'''

class People:
    # 基本属性
    name = ''
    age = 1

    # 定义私有变量
    _weight = 0

    # 构造方法
    def __init__(self, name, age, weight):
        self.name = name
        self.age = age
        self._weight = weight

    #method:
    def speak(self):
        print('{0}说: 我{1}岁了'.format(self.name, self.age))
        # return self




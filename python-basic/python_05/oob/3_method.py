#!/usr/bin/env python3
#coding:utf-8

'''
在类中, 使用def来定义方法, 但必须有个self参数\
变量直接用就ok
私有变量前面加 __
私有方法前面加 __

'''

from oob import People

p = People('vini', 25, 48)
p.speak()



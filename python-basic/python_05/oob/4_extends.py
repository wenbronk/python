#!/usr/bin/env python3
#coding:utf-8

'''
python 的 单继承, 使用 () 来继承

调用父类的构造方法  People.__init__(self, *args)

'''

# from oob import People


class People:
    # 基本属性
    name = ''
    age = 1

    # 定义私有变量
    _weight = 0

    # 构造方法
    def __init__(self, name, age, weight):
        self.name = name
        self.age = age
        self._weight = weight

    #method:
    def speak(self):
        print('{0}说: 我{1}岁了'.format(self.name, self.age))
        return self

# 单继承实例
class Student(People):
    grade = ''
    def __init__(self, name, age, weight, grade):
        # 调用父类构造函数
        People.__init__(self, name, age, weight)
        self.grade = grade

    def speak(self):
        print('{0}说, 我今年{1}岁, 我在都{2}年纪'.format(self.name,
            self.age, self.grade))
        return self

student = Student('vini', 25, 48, '3')
student.speak().speak()



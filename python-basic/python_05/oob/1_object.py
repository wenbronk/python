#!/usr/bin/env python3
#coding:utf-8

'''
python 的面向对象

class: 用来描述具有相同属性和方法的集合, 对象是类的实例
类变量:
实例变量:
数据成员: 类变量或实例变量用于处理类及实例对象的相关数据

重写:
继承: 可以继承多个基类, 派生类可以覆盖基类中的任何方法, 方法可以调用基类中的同名方法
实例化:
方法:
对象:


专有方法:
__init__: 构造
__del__: 析构函数, 释放函数使用
__repr__: 打印, 转换
__setitem__: 按照索引复制
__getitem__: 按照索引取值
__len__:     获取长度
__cmp__:    比较运算
__call__: 函数调用
__add__:     加运算
__sub__:       减运算
__mul__:        乘运算
__div__:        除运算
__mod__:        求余运算
__pow__:        乘方

'''

# 一个简单的对象
class MyClass:
    i = 12345
    def f (self):
        return 'hello world'
# 将类实例化
x = MyClass()

# 访问属性及方法
print('Myclasss 属性为: ', x.i)
print('function 为: ', x.f())






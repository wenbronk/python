#!/usr/bin/env python3
#coding:utf-8

'''
方法的重写

'''

class Parent:
    def myMethod(self):
        print('钓鱼那个')


# 子类继承重写父类的方法
class Child(Parent):
    def myMethod(self):
        print('i am override method')


c = Child()
c.myMethod()

#!/usr/bin/env python3
#coding:utf-8

'''
python 中的私有方法和变量, 都用 __作为前缀来命名

'''

class JustCounter:

    # 私有变量
    __secretCount = 0
    publicCount = 0

    def count(self):
        self.__secretCount += 1
        return self.__secretCount

    def __add(self):
        self.publicCount += 1


counter = JustCounter()
print(counter.publicCount)
print(counter.count())





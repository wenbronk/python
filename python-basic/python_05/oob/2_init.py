#!/usr/bin/env python3
#coding:utf-8

'''
python中的construct为 __init__
self 和 java中的this一样, 指代实例, 而不是类
self.class 指类

'''

# 使用init进行初始化的
class Complex:
    def __init__(self, realpart, imagepart):
        self.realpart = realpart
        self.imagepart = imagepart

    # self 指代类
    def prf(self):
        print(self)
        print(self.__class__)

x = Complex(3, 4)
print(x.realpart, x.imagepart)
x.prf()



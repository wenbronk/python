#!/usr/bin/env python3
#coding:utf-8

'''
标准库
os模块: 建议使用import os, 而不是 from os import *, 放置被覆盖

'''

# os模块
import os
print(os.getcwd())          # 返回当前工作目录
os.chdir('../../')          # 修改当前的工作目录
os.system('mkdir today')    # 创建目录

# :mode:shutil 提供了一个简单的高级接口用于文件和目录管理
# import shutil
# shutil.copyfile('__init__.py', './123.py')
# shutil.move('../build/executables', 'installdir')

# glob 模块方便的从目录中搜索成文件列表
import glob
print(glob.glob('*.py'))

# 输出重定向, stdin, stdout, stderr, 使用异步输出的
import sys
sys.stderr.write("warning...")

# match 模块提供了对底层c的访问:
import math
print(math.cos(math.pi / 4))

#random 生成随机数
import random
print(random.choice(['apple', 'pear', 'banana']))
print(random.randrange(6))










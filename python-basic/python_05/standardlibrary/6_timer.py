#!/usr/bin/env python3
#coding:utf-8

'''
python的性能度量工具

'''

from timeit import Timer
a = Timer('t=a; a=b; b=t', 'a=1; b=2').timeit()

b = Timer('a,b = b, a', 'a=1; b=2').timeit()

print(a, ', ', b)



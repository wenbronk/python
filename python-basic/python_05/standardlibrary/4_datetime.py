#!/usr/bin/env python3
#coding:utf-8

'''
日期和时间模块


'''

from datetime import date
# 获取时间
now = date.today()
print(now, ', ')

# 设置时间
now.strftime('%m-%d-%y is a %A on the %d day of %B')
birthday = date(1926, 7, 31)
age = now - birthday
days = age.days
print(days)

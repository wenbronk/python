#!/usr/bin/env python3
#coding:utf-8

'''
包采用 点模块名称

模块的名称是 A.B, 那么标识是包A 中的子模块 B

目录中只有 包含 __init__ 文件, 才能被称作一个包
    可包含一些初始化代码或者 为 __all__ 的变量赋值
'''


# 指导入一个包的特定模块, 必须使用权路径来访问
import package.format.echo
package.format.echo.echo()

import module.fibo
module.fibo.fib(10)


# 或者使用from来导入, module里面已经有了

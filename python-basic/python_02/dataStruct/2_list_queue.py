#!/usr/bin/env python3
#coding:utf-8

'''
列表推到式, 从序列简单的创建列表
将操作应用于序列的每个元素, 将其结果作为新的列表元素
            需要用 [], 元组用()
每个列表推到是都在for之后创建一个表达式, 然后有0-多个 for或if子句, 返回结果
    是一个根据表达式从背后的for和if上下文环境中生成的列表, 如果希望推到出一个元组, 必须用口号

'''

# ×3 获取一个新的列表
vec = [2, 4, 6]
new_vec = [3 * x for x in vec]
print(new_vec)

# 获取一个map
dict = [(x, x**2) for x in vec]
print(dict)

# 对序列中元素租个调用
fruits = ['banana', 'apple', 'oragin']
new_fruits = [fruit.upper() for fruit in fruits]
print(new_fruits)

# 使用if
if_vec = [3 * x for x in vec if x > 3]
print(if_vec)

# 2个列表混搭的
vec2 = [4, 6, -1]
lista = [x * y for x in vec for y in vec2]
listb = [vec[i] * vec2[i] for i in range(len(vec))]

print(lista)
print(listb)


#!/usr/bin/env python3
#coding:utf-8

'''
list的数据结构
append(value)
extend(L)   添加指定列表的所有元素
insert(index, value)    插入
remove(value)
pop(index)        不指定删除最后一个,
clear()         清空
count(value)    统计有多少个
sort()          排序
reverse()       到序
copy()          潜复制

del 语句
使用索引删除
'''

# 把列表当堆栈使用, 先进先出
stack = [3, 4, 5]
stack.append(6)
stack.append(7)
a = stack.pop()
b = stack.pop()
print(a, b)


# 当队列使用, 但这样效率不高
from collections import deque
queue = deque(['e', 'j', 'm'])
queue.append('t')
queue.append('g')
c = queue.popleft()
d = queue.popleft()
print(c, d)


#[::]的使用
list = range(1, 10)
print(list[::2])    # 从第一个开始, 隔2个取一个
print(list[2::])    # 从index=2 开始取, 到最后
print(list[::-1])   # 跟reverse一样, 倒叙
print(list[::-2])   # 倒序, 隔2个取一个


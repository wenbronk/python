#!/usr/bin/env python3
#coding:utf-8

'''
嵌套列表转换
'''


# 3 * 4 转变为 4 * 3
matrix = [
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 11, 12]
]

new_matrix = [[row[i] for row in matrix] for i in range(4)]
print(new_matrix)

# 上面的相当于
matrix_1 = []
for i in range(4):
    matrix_1.append([row[i] for row in matrix])
print(matrix_1)

# 获取进一步改写
matrix_2 = []
for i in range(4):
    ineer = []
    for row in matrix:
        ineer.append(row[i])
        print(row[i], end=', ')
    print('---', row)
print(matrix_2)

for row in matrix:
    print(row)
# print([row for row in matrix])

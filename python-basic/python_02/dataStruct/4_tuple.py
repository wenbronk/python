#!/usr/bin/env python3
#coding:utf-8

'''
元组由一系列逗号分隔的值组成
'''

# 创建时可以没有括号, 但在括号是必须的
a = 123, 234, 4532
print(a)

b = a, (2,3,4,5)
print(b)
#!/usr/bin/env python3
#coding:utf-8

'''
集合的遍历
'''

# 字典 推导
new_dict = {x + 10: x**2 + 10 for x in range(10)}
print(new_dict)

# 字典的遍历
for k, v in new_dict.items():
    print(k, v)
# 返回索引的, dict返回key
for i, v in enumerate(new_dict):
    print(i, v)

# 遍历2个货更多的序列, 使用 zip(), 去最短的序列
questions = ['name', 'quest', 'favorite']
answers = ['lancelot', 'the', 'blue', 'abc']

for q, a in zip(questions, answers):
    print(q, a)

# 反向遍历集合
for q in reversed(questions):
    print(q)





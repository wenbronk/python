#!/usr/bin/env python3
#coding:utf-8

'''
可变参数
'''


#不定长参数
def functionName(*vartuple):
    # print(arg1)

    for var in vartuple:
        print(var)
    return
functionName(10)
functionName(10, 50, 70, 90)


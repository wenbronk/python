#!/usr/bin/env python3
#coding:utf-8

'''
变量的作用范围, L, local, E 闭包函数外, G global, B 内建作用域
    只有模块, 类, 函数, 才会引入新的作用域, if, try, for不会引入新的作用域

global:
    内部想要修改全局变量时, 使用global声明
nonlocal:
    内部要修改非全局变量时, 使用nonlocal声明


'''

# global关键字, 修改全局变量

F = print
num = 3
def sum():

    # global num
    num = 5
sum()
F(num)

#nonlocal修改局部变量
def outer():
    num = 10
    def inner():
        # nonlocal num
        num = 20
        F(num)
    inner()
    print(num)
outer()


#!/usr/bin/env python3
#coding:utf-8

'''
匿名函数
python 使用lambda表达式创建匿名函数
    主题是表达式, 能封装有限的逻辑

'''

sum = lambda arg1, arg2: arg1 + arg2


print('相加后的值为: ', sum(1, 3))



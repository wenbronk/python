#!/usr/bin/env python3
#coding:utf-8

'''
函数, 有参数输入, 然后又值返回

关键字作为参数, 必须传入一个要求类型的变量才可
有关键字时, 不需要指定顺序, 只要指明即可
'''

def hello(width, height):
    print('hello')
    return width * height

result = hello(3, 5)
print(result)

# 参数传递
# python中, 变量没有类型, 类型是属于对象的
# 分值传递和引用传递
#!/usr/bin/env python3
#coding:utf-8

'''
指定默认值
'''

# 指定默认值
def printInfo(name, age=15):
    print('name: ', name)
    print('age', age)
    return

printInfo(name='runoob', age=50)
#!/usr/bin/env python3
#coding:utf-8

'''
每个模块都有一个 __name__ 属性, 当值为 __main__ 时, 表明在本模块运行

'''

if __name__ == '__main__':
    print('在主模块中运行')
else:
    print('在其他模块中运行')


print(dir())
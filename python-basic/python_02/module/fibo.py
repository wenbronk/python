#!/usr/bin/env python3
#coding:utf-8

'''
费波拉契数列模块, 用于导入
'''

def fib(n):
    a, b = 0, 1
    while b < n:
        print(b, end=' ')
        a, b = b, b + a

def fib2(n):
    result = []
    a, b = 0, 1
    while b < n:
        result.append(b)
        a, b = b, a + b
    return result



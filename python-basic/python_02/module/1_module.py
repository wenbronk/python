#!/usr/bin/env python3
#coding:utf-8

'''
一个文件被称作一个模块, 包含定义的函数和变量, .py文件
可被别的模块引用, 也可用引入别的模块

import 导入的是根目录开始的, 如果有包, 则用from import 导入

'''

# 使用import 导入模块
import sys
print('命令行参数如下: ')
for i in sys.argv:
    print(i)

print(sys.path)


# 使用from导入, from可以从模块中导入一个指定的部分
from module import support
support.print_func('123')

# 导入fibo模块
from module import fibo
fibo.fib(10)
print()
result = fibo.fib2(10)
print('aaa: ', result)


# 使用from 导入一部分
from fiboooooo import fib
fib(10)



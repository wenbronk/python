#!/usr/bin/env python
#coding:utf-8

'''
for循环中使用else语句
会在for执行完成后, 不是通过break终端的情况下执行
'''

for num in range(10, 20):
    for i in range(2, num):
        if num % i == 0:
            j = num / i
            print ('%d 等于 %d * %d') % (num, i, j)
            break
    else :
        print('质数', num)


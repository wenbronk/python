#!/usr/bin/env python
#coding:utf-8

'''
for循环的使用

'''

# 通过索引迭代
fruits = ['banana', 'apple', 'mongo']

for index in range(len(fruits)):
    print(fruits[index])
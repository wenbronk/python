#!/usr/bin/env python
#coding:utf-8

'''
使用内置的enumerate进行遍历
'''

for index, item in enumerate(['vini', 'bronk', 'how?']):
    print(index, item)
#!/usr/bin/env python3
#coding:utf-8

'''
end关键字的使用
将结果输出到同一行, 或者在输出末尾添加字符
'''

a = 0
b = 1
while b < 10:
    print(b, end=', ')
    a = b
    b += a
    print(b)



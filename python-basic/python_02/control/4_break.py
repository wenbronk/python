#!/usr/bin/env python
#coding:utf-8

'''
break 同java, 退出最近的循环
'''


for letter in 'Python':
    if letter == 'h':
        break
    print('pop',letter)
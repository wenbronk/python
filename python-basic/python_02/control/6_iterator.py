#!/usr/bin/env python3
#coding:utf-8

'''
迭代器和生成器
iter(), next()
'''

list = [2, 5, 7, 8]
it = iter(list)

print(it)       # 打印地址

for x in it:
    print(x)


# 使用while读取
import sys
it2 = iter(list)
while True:
    try:
        print(next(it2))
    except StopIteration:
        sys.exit()



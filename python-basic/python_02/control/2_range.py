#!/usr/bin/env python3
#coding:utf-8

'''
range() 用来生成数列
'''

# 指定起始和终止点
for i in range (5, 10):
    print(i)

print('-----')

#指定增量
for i in range(0, 10, 3):
    print(i)

# 遍历一个序列的索引
a = ['a', 'aa', 'aaa', 'aaaa']
for index in range(len(a)):
    print(a[index])
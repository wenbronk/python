#!/usr/bin/env python3
#coding:utf-8

'''
python中, 使用了yield的函数被称为生成器
生成器是一个返回迭代器的函数, 只能用于迭代操作
'''

import sys

def fibonacci(n):
    a, b, count = 0, 1,0
    while True:
        if (count > n):
            return
        yield a
        a, b = b, a + b
        count += 1
f = fibonacci(10)

while True:
    try:
        print(next(f))
    except StopIteration:
        sys.exit()


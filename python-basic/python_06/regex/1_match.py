#!/usr/bin/env python3
#coding:utf-8

#author by: wenbronk

'''
正则表达式, match函数

re.match(patter, string, flags=0
flags 标志位, 是否区分大小写, 多行匹配等

search(patter, string, flags=0) 扫描真个文档并返回第一个匹配
match 和 search 的区别是, match从开始匹配, 开始不符合正则, 则匹配失败, 返回None
         search匹配整个字符串, 直到找到一个匹配

sub(pattern, repl, string, count=0)
    repl: 要替换的字符串, 或者一个函数
    count: 模式匹配后替换的最大次数, 默认0替换所有的匹配

正则的修饰符:
re.I	使匹配对大小写不敏感
re.L	做本地化识别（locale-aware）匹配
re.M	多行匹配，影响 ^ 和 $
re.S	使 . 匹配包括换行在内的所有字符
re.U	根据Unicode字符集解析字符。这个标志影响 \w, \W, \b, \B.
re.X	该标志通过给予你更灵活的格式以便你将正则表达式写得更易于理解。

'''

# match 函数
import re
print(re.match('www', 'www.runoob.com').span())
print(re.match('com', 'www.runoob.com'))

line = 'Cats are smarter than dogs'
matchObj = re.match(r'(.*) are (.*?) .*', line, re.M|re.I)
if matchObj:
    print('matchObj.group(): ', matchObj.group())
    print('matchObj.group(1): ', matchObj.group(1))
    print('matchObj.group(2): ', matchObj.group(2))
else:
    print('No match!')

# search() 扫描整个字符串并返回第一个成功的匹配
print(re.search('www', 'www.runoob.com').span())
print(re.search('com', 'www.runoob.com').span())

line = 'Cats are smarter than dogs'
searchObje = re.search(r'(.*) are (.*?) .*', line, re.M|re.I)
if matchObj:
    print('searchObje.group(): ', searchObje.group())
    print('searchObje.group(1): ', searchObje.group(1))
    print('searchObje.group(2): ', searchObje.group(2))
else:
    print('No match!')

    # search 和match的区别
matchObj = re.match( r'dogs', line, re.M|re.I)
if matchObj:
   print ("match --> matchObj.group() : ", matchObj.group())
else:
   print ("No match!!")

matchObj = re.search( r'dogs', line, re.M|re.I)
if matchObj:
   print ("search --> matchObj.group() : ", matchObj.group())
else:
   print ("No match!!")


# 检索和替换
phone = "2004-959-559 # 这是一个电话号码"
# 删除注释
num = re.sub(r'#.*$', "", phone)
print ("电话号码 : ", num)
# 移除非数字的内容
num = re.sub(r'\D', "", phone)
print ("电话号码 : ", num)

# 获取repl的参数是一个函数
# 将匹配的数字乘于 2
def double(matched):
    value = int(matched.group('value'))
    return str(value * 2)

s = 'A23G4HFD567'
print(re.sub('(?P<value>\d+)', double, s))
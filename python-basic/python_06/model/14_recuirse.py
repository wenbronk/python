#!/usr/bin/env python3
#coding:utf-8

#author by: wenbronk

'''
递归费波拉契数列

'''

def recur_fibo(n):
    if n <= 1:
        return n
    else:
        return recur_fibo(n-1) + recur_fibo(n - 2)

num = 7
print(recur_fibo(num))

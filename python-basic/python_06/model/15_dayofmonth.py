#!/usr/bin/env python3
#coding:utf-8

#author by: wenbronk

'''
计算每个月的天数

'''

import calendar
monthRange = calendar.monthrange(2016, 9)

# 输出一个元祖, 第一个对应星期几, 第二个是这个月多少天
print(monthRange)


# 获取昨天的日志
import datetime
def getYesterday():
    today = datetime.date.today()
    oneday = datetime.timedelta(days = 1)
    yesterday = today - oneday
    return yesterday
print(getYesterday())

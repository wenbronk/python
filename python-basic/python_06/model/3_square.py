#!/usr/bin/env python3
#coding:utf-8

'''
平方根
'''


num1 = 234
num2 = 12

square = num1 ** 0.5
square2 = num2 ** 0.5

print(square, ", ", square2)


# 或者导入cmatch库
import cmath
print(cmath.sqrt(num1))


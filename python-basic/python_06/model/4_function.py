#!/usr/bin/env python3
#coding:utf-8

'''
二次方程求解

'''

import cmath

a = float(input('input a: \n'))
b = float(input('input b: \n'))
c = float(input('input c: \n'))

d = (b ** 2) - 4 * a * c
sol1 = (-b - cmath.sqrt(d)) / (2 * a)
sol2 = (-b + cmath.sqrt(d)) / (2 * a)

print(sol1, ', ', sol2)


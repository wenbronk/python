#!/usr/bin/env python3
#coding:utf-8

'''
交换变量

'''

a = 3
b = 5

# 不实用临时变量
b, a = a, b
print(a, ', ', b)

#!/usr/bin/env python3
#coding:utf-8

'''
判断是否是数字

'''

def is_number(s):
    try:
        float(s)
    except ValueError:
        pass

    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass

    return False



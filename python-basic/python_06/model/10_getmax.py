#!/usr/bin/env python3
#coding:utf-8

#author by: wenbronk

'''
获取最大值函数

'''


print(max(1, 2))
print(max('a', 'b'))

# 可以对元组操作
print(max([1, 2]))
print(max((1, 2)))


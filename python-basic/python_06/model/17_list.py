#!/usr/bin/env python3
#coding:utf-8

#author by: wenbronk

'''
list 常用操作

'''

# 定义
li = ['a', 'b', 'mpilgrim', 'z', 'example']
#       0   1        2        3     4
#       -5   -4     -3        -2     -1

# 通过角标获取元素
print(li[3])

# 负角标
print(li[-3])
print(li[1:3])
print(li[1: -1])
print(li[1::2])

# 增加元素
li.append('new')
li.insert(2, 'list')
li.extend(['a', 'c'])
print(li)

# list搜索, index没有回报错
index = li.index('new')
print(index)
print('e' in li)

# 删除元素, 删除首次出现的第一个值, 没有, 也会报错
li.remove('new')
if ('e' in li):
    li.remove('e')

# list运算, 不会改变原来的串, 会生成一个新串
li = ['a', 'b', 'mpilgrim']
la = li + ['example', 'new']
print(li)
print(li * 3)   # 复制3次

# list 和str 之间的转化
str = ' '.join(li)
print(str)

lb = str.split(' ')
print(lb)

# 导出列表
li = [1, 9, 8, 4]
lc = [elem * 2 for elem in li]
print(lc)

# dict中的导出列表操作
params = {'server':'iwhere', 'database':'mapmatch', 'uid':'vini'}
print(params.keys())
print(params.values())
print(params.items())
lz = [k for k, v in params.items()]
print(lz)

ly = ['%s=%s' % (k, v) for k, v in params.items()]
print(ly)

# list过滤
ll = ["a", "mpilgrim", "foo", "b", "c", "b", "d", "d"]
le = [elem for elem in ll if len(elem) > 1]
print(le)

ln = [elem for elem in ll if elem != 'b']
print(ln)

lc = [elem for elem in li if li.count(elem) == 1]
print(lc)


#!/usr/bin/env python3
#coding:utf-8

#author by: wenbronk

'''

判断质数

'''

num = int(input('请输入一个数字: '))

if num > 1:
    for i in range(2, num):
        if num % i == 0:
            print('{0}不是质数, {1} * {2} = {0}'.format(num, i, num / i))
            break
    else:
        print(num, '是质数')
else:
    print(num, '不是质数')


# 输出给定范围内的质数
min = int(input('输入最小'))
max = int(input('输入最大'))

for abc in range(min, max):
    if abc > 1:
        for i in range(2, abc):
            if abc % i == 0:
                break
        else:
            print(abc)


#
__author__='wenbronk'

import os

database_list=['dwd', 'dws', 'dwa']
dirPath = "/home/data_platform/wenbronk/schema/tables/"


for db in database_list:
    table_list = os.popen("hive -e 'use %s; show tables '" % (db)).read()

    for table_name in table_list.split('\n'):
        if table_name.strip() !='' :
            print("get table %s, %s" % (db, table_name))
            fileObj = open("%s%s.%s" % (dirPath, db, table_name), "a+")
            table_create = os.popen("hive -e 'show create table %s.%s'" % (db, table_name)).read()
            fileObj.write(table_create)
            fileObj.close()
            pass

    pass



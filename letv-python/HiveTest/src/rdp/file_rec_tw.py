# -*- coding: UTF-8 -*-
__author__ = 'wenbronk'

import os
import re

dirPath = "/home/data_platform/wenbronk/schema/tables/"

shemaPath = '/home/data_platform/wenbronk/schema/file/'
erroFile = open('/home/data_platform/wenbronk/schema/error', 'a+')

i = 0
j = 1

for root, dirs, files in os.walk(dirPath):
    for fileName in files:
        print("deal with .." + root + fileName)
        srcPath = open(root + fileName, 'r+')
        lines = srcPath.read().decode('utf-8').replace('\n', '#$#')
        match = re.match(".*?\((.*?)\).*", lines)

        if (match):

            i += 1
            if (i >= 20) :
                i = 0
                j += 1

            dscPath = open(shemaPath + str(j) + ".csv", 'a+')
            dscPath.write(fileName + " -- desc\n")
            dscPath.write("name,colume,type\n")
            rowList = match.group(1).split('#$#')

            for row in rowList:
                if (row.strip() != '') :

                    print(row)
                    columes = row.strip().split(" ")
                    print(columes)

                    fileName = columes[0].strip()
                    mat1 = re.match(".*?`(.*?)`.*", fileName)
                    if (mat1):
                        fileName = mat1.group(1)

                    if len(columes) >= 4:

                        comment = columes[3]
                        mat2 = re.match(".*?'(.*?)'.*", comment)
                        if (mat2):
                            comment = mat2.group(1)

                        dscPath.write(("%s,%s,%s\n" % (comment, fileName, columes[1])).encode("utf-8-sig"))
                    else:
                        dscPath.write(" ,%s,%s\n" % (fileName, columes[1]))
            dscPath.close()
        else:
            erroFile.write(srcPath + '\n')
erroFile.close()